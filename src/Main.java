import java.util.Scanner;

public class Main {

	public static Scanner kb = new Scanner(System.in);

	public static int endgame = 0;
	public static int count = 1;
	public static int turn = 0;
	public static char player = ' ';
	public static int r = 0;
	public static int c = 0;
	public static int row;
	public static int col;

	public static char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };

	private static void showWelcome() {
		System.out.println("**********WELCOME TO THE GAME**********");
		System.out.println();
	}

	private static void showTable() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print("|");
				System.out.print(table[i][j]);
			}
			System.out.print("|");
			System.out.println();
		}
		System.out.println();
	}

	private static void showInput() {
		System.out.print("INPUT POSITION : ");
		row = kb.nextInt() - 1;
		col = kb.nextInt() - 1;
	}
	
	private static void numTurn() {
		System.out.println("TURN	" + (count));
	}
	
	private static void select() {
		System.out.println();
		System.out.println("PLAYER SELECT O OR X");
		System.out.print("YOU CHOOSE ");
		player = kb.next().charAt(0);
		System.out.println();
		if( player == 'O' ) {
			turnO();
		}else {
			turnX();
		}
	}

	private static void turnO() {
		showTable();
		numTurn();
		System.out.println("MY TURN O");
		showInput();
		if (row > 2 || row < 0 || col > 2 || col < 0 || table[row][col] == 'O' || table[row][col] == 'X') {
			checkError(row, col, table);
			turnO();
		} else {
			table[row][col] = 'O';
			count++;
			if (count == 9) {
				checkTie();
			}
			checkOWin();
		}
		if (endgame != 1) {
			turnX();
		}
	}

	private static void turnX() {
		showTable();
		numTurn();
		System.out.println("MY TURN X");
		showInput();
		if (row > 2 || row < 0 || col > 2 || col < 0 || table[row][col] == 'O' || table[row][col] == 'X') {
			checkError(row, col, table);
			turnX();
		} else {
			table[row][col] = 'X';
			count++;
			if (count == 9) {
				checkTie();
			}
			checkXWin();
		}
		if (endgame != 1) {
			turnO();
		}
	}

	private static void checkError(int row, int col, char[][] TableOX) {
		if (row > 2 || row < 0) {
			System.out.println("ERROR!!! INPUT 1 2 OR 3");
			System.out.println();
		} else if (col > 2 || col < 0) {
			System.out.println("ERROR!!! INPUT 1 2 OR 3");
			System.out.println();
		} else if (table[row][col] == 'O' || table[row][col] == 'X') {
			System.out.println("IT HAS ALREADY BEEN SELECTED.");
			System.out.println();
		}
	}

	private static void checkOWin() {
		if ((table[r][c] == 'O' && table[r][c + 1] == 'O' && table[r][c + 2] == 'O')
				|| (table[r][c] == 'O' && table[r + 1][c] == 'O' && table[r + 2][c] == 'O')
				|| (table[r][c] == 'O' && table[r + 1][c + 1] == 'O' && table[r + 2][c + 2] == 'O')
				|| (table[r + 1][c] == 'O' && table[r + 1][c + 1] == 'O' && table[r + 1][c + 2] == 'O')
				|| (table[r + 2][c] == 'O' && table[r + 2][c + 1] == 'O' && table[r + 2][c + 2] == 'O')
				|| (table[r][c + 1] == 'O' && table[r + 1][c + 1] == 'O' && table[r + 2][c + 1] == 'O')
				|| (table[r][c + 2] == 'O' && table[r + 1][c + 2] == 'O' && table[r + 2][c + 2] == 'O')
				|| (table[r][c + 2] == 'O' && table[r + 1][c + 1] == 'O' && table[r + 2][c] == 'O')) {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					System.out.print("|");
					System.out.print(table[i][j]);
				}
				System.out.print("|");
				System.out.println();
			}
			System.out.println();
			showOWin();
		}

	}

	private static void checkXWin() {
		if ((table[r][c] == 'X' && table[r][c + 1] == 'X' && table[r][c + 2] == 'X')
				|| (table[r][c] == 'X' && table[r + 1][c] == 'X' && table[r + 2][c] == 'X')
				|| (table[r][c] == 'X' && table[r + 1][c + 1] == 'X' && table[r + 2][c + 2] == 'X')
				|| (table[r + 1][c] == 'X' && table[r + 1][c + 1] == 'X' && table[r + 1][c + 2] == 'X')
				|| (table[r + 2][c] == 'X' && table[r + 2][c + 1] == 'X' && table[r + 2][c + 2] == 'X')
				|| (table[r][c + 1] == 'X' && table[r + 1][c + 1] == 'X' && table[r + 2][c + 1] == 'X')
				|| (table[r][c + 2] == 'X' && table[r + 1][c + 2] == 'X' && table[r + 2][c + 2] == 'X')
				|| (table[r][c + 2] == 'X' && table[r + 1][c + 1] == 'X' && table[r + 2][c] == 'X')) {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					System.out.print("|");
					System.out.print(table[i][j]);
				}
				System.out.print("|");
				System.out.println();
			}
			System.out.println();
			showXWin();
		}
	}

	private static void checkTie() {
		if (count == 9) {
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					System.out.print("|");
					System.out.print(table[i][j]);
				}
				System.out.print("|");
				System.out.println();
			}
			System.out.println();
			System.out.println("IT IS TIE");
			System.out.println("**********GAME OVER**********");
			endgame++;
		}
	}

	private static void showOWin() {
		System.out.println("PLAYER O WIN");
		System.out.println();
		System.out.println("**********GAME OVER**********");
		endgame++;
	}

	private static void showXWin() {
		System.out.println("PLAYER X WIN");
		System.out.println();
		System.out.println("**********GAME OVER**********");
		endgame++;
	}

	public static void main(String[] args) {
		showWelcome();
		select();
	}
}

